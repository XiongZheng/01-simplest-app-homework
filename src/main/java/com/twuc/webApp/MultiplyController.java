package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MultiplyController {
    @GetMapping(value = "/api/tables/multiply")
    public String multiply() {
        StringBuilder s = new StringBuilder();
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j <= i; j++) {
                s.append(i).append("*").append(j).append("=").append(i * j);
                if (i * j > 9) {
                    s.append(" ");
                } else {
                    s.append("  ");
                }
            }
            s.append("\n");
        }
        return s.toString();
    }
}
