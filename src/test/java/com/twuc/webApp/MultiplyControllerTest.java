package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MultiplyControllerTest {

    @Autowired
    public MockMvc mockMvc;

    @Test
    void should_return_multiply_table() throws Exception {
        mockMvc.perform(get("/api/tables/multiply"))
                .andExpect(content().string(
                        "1*1=1  \n" +
                                "2*1=2  2*2=4  \n" +
                                "3*1=3  3*2=6  3*3=9  \n" +
                                "4*1=4  4*2=8  4*3=12 4*4=16 \n" +
                                "5*1=5  5*2=10 5*3=15 5*4=20 5*5=25 \n" +
                                "6*1=6  6*2=12 6*3=18 6*4=24 6*5=30 6*6=36 \n" +
                                "7*1=7  7*2=14 7*3=21 7*4=28 7*5=35 7*6=42 7*7=49 \n" +
                                "8*1=8  8*2=16 8*3=24 8*4=32 8*5=40 8*6=48 8*7=56 8*8=64 \n" +
                                "9*1=9  9*2=18 9*3=27 9*4=36 9*5=45 9*6=54 9*7=63 9*8=72 9*9=81 \n"))
        .andExpect(status().is(200))
        .andExpect(content().contentTypeCompatibleWith("text/plain"));
    }
}
